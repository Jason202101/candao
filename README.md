# 餐道聚合配送SDK



[TOC]

### 起步

- 引入：use candao\Candao;
- New对象：$candao = new Candao("60cf881fb91bff19", "b546af87d901cde82c29487372a30c13", "26020535", "餐道测试店", "http://zt-beta.can-dao.com:81/api");

- 调用方法：

  ```
  //查询门店详情
  var_dump($candao->getShopDetail());
  ```

### 方法

```php
<?php
namespace candao;

/**
 * 餐道聚合配送类
 * Created by Jason
 * Copyright: Opensource
 * Date：2021/8/23
 * Time: 9:30
 */

ini_set('date.timezone','Asia/Shanghai');
class Candao{

    private $AccessKey;
    private $AccessSecret;
    private $HttpUrl = "https://openapi.can-dao.com/api";

    //构造传参
    public function __construct($AccessKey, $AccessSecret)
    {
        $this->AccessKey = $AccessKey;
        $this->AccessSecret = $AccessSecret;
    }

    //CURL方法
    public function getInit($url, $method='GET', $data=null){
        $curl = curl_init();
        $agent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.3623SE 2.X MetaSr 1.0'";

        curl_setopt($curl, CURLOPT_HTTPHEADER, ["content-type:application/json;charset:utf-8", "accept:application/json"]);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_USERAGENT, $agent);
        if($method != "GET"){
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }

    //获取当前毫秒时间戳
    public function getMillisecond(){
        list($msec, $sec) = explode(' ', microtime());

        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);

        return $msectimes = substr($msectime,0,13);

    }

    //生成UUID
    public function generateUUID(){
        $chars = md5(uniqid(mt_rand(), true));
        $uuid = substr ( $chars, 0, 8 ) . '-'
            . substr ( $chars, 8, 4 ) . '-'
            . substr ( $chars, 12, 4 ) . '-'
            . substr ( $chars, 16, 4 ) . '-'
            . substr ( $chars, 20, 12 );
        return $uuid;
    }

    //查询门店详情
    public function getShopDetail($subStoreId){
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.store.getStoreInfo";
        $sendData['serviceType'] = "pos";

        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));
        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //同步门店信息 -- 新增门店
    public function addStoreInfo($brandName, $subStoreId, $storeName, $provinceId, $cityId, $districtId, $storeAddress, $latitude, $longitude, $phoneList, $coordinate){
        $sendData['data']['type'] = 'add';
        $sendData['data']['data']['storeName'] = $storeName;
        $sendData['data']['data']['brandName'] = $brandName;
        $sendData['data']['data']['onLinePayType'] = [2];
        $sendData['data']['data']['subStoreId'] = $subStoreId;
        $sendData['data']['data']['provinceId'] = $provinceId;
        $sendData['data']['data']['cityId'] = $cityId;
        $sendData['data']['data']['districtId'] = $districtId;
        $sendData['data']['data']['storeAddress'] = $storeAddress;
        $sendData['data']['data']['latitude'] = $latitude;
        $sendData['data']['data']['longitude'] = $longitude;
        $sendData['data']['data']['phoneList'] = $phoneList;
        $sendData['data']['data']['businessTimes'][] = [
            "beginTime" => "00:00",
            "endTime" => "23:59",
        ];
        $sendData['data']['data']['orderType'] = [1];
        $sendData['data']['data']['payType'] = [2];
        $sendData['data']['data']['isInvoice'] = false;
        $sendData['data']['data']['eleDelivery'] = false;
        $sendData['data']['data']['autoChangeDeliver'] = false;
        $sendData['data']['data']['deliveryRanges'][]['coordinate'] = $coordinate;
        $sendData['data']['data']['deliveryRanges'][]['rangeName'] = "配送范围";
        $sendData['data']['data']['deliveryRanges'][]['rangeType'] = 1;
        $sendData['data']['data']['deliveryRanges'][]['deliveryTimes'][] = [
            "beginTime"=>"00:00",
            "endTime"=>"23:59"
        ];
        $sendData['data']['data']['deliveryRanges'][]['weight'] = 1;
        $sendData['data']['data']['deliveryRanges'][]['deliveryType'] = 2;
        $sendData['data']['data']['deliveryRanges'][]['reachFee'] = 0;
        $sendData['data']['data']['deliveryRanges'][]['deliveryFee'] = 10;


        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.store.postStoreInfo";
        $sendData['serviceType'] = "pos";

        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));
        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //推送新订单
    public function pushOrder($subStoreId, $extOrderId, $longitude, $latitude, $name, $phone, $address, $products){
        $sendData = [];
        $sendData['data']['extOrderId'] = $extOrderId;
        $sendData['data']['longitude'] =  $longitude;
        $sendData['data']['latitude'] = $latitude;
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['data']['name'] = $name;
        $sendData['data']['phone'] = $phone;
        $sendData['data']['address'] = $address;
        $sendData['data']['orderType'] = 1;
        $sendData['data']['orderTime'] = date("Y-m-d H:i:s",time());
        $sendData['data']['payType'] = 2;
        $sendData['data']['orderStatus'] = 10;
        $sendData['data']['isPayed'] = true;
        $sendData['data']['isInvoice'] = false;
        $sendData['data']['price'] = 106.87;
        $sendData['data']['merchantPrice'] = 106.00;
        $sendData['data']['ftType'] = ["emptyProductId"=>true,"productAberrant"=>true,"storeAberrant"=>true];
        $sendData['data']['products'] = $products;

        $sendData['accessKey'] =  $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.order.pushOrder";
        $sendData['serviceType'] = "pos";

        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));
        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        var_dump($data);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //查看订单详情
    public function getOrderDetail($orderId){
        $sendData['data']['orderId'] = $orderId;
        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.order.getOrderDetail";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //同步订单状态 -- 取消
    public function updateOrderStatus($orderId, $subStoreId, $status, $cancelReason){
        $sendData['data']['orderId'] = $orderId;
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['data']['status'] = $status;
        $sendData['data']['cancelReason'] = $cancelReason;
        $sendData['data']['updateTime'] = date("Y-m-d H:i:s",time());

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.order.updateOrderStatus";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        var_dump($data);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //预览配送费
    public function previewDeliveryFee($storeName, $subStoreId, $orderId, $deliveryOrderId, $sdName, $sdPhone, $address, $sdLongitude, $sdLatitude, $rcName, $rcPhone,
                                       $rcAddress, $rcLongitude, $rcLatitude, $productPrice, $deliveryFee, $mealFee, $price, $merchantPrice, $discountPrice, $weight, $products){
        $sendData['data']['deliverySysType'] = "foops";
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['data']['orderId'] = $orderId;
        $sendData['data']['deliveryOrderId'] = $deliveryOrderId;
        $sendData['data']['storeName'] = $storeName;
        $sendData['data']['sender']['name'] = $sdName;
        $sendData['data']['sender']['phone'] = $sdPhone;
        $sendData['data']['sender']['address'] = $address;;
        $sendData['data']['sender']['longitude'] =  $sdLongitude;;
        $sendData['data']['sender']['latitude'] = $sdLatitude;;
        $sendData['data']['receiver']['name'] = $rcName;
        $sendData['data']['receiver']['phone'] = $rcPhone;
        $sendData['data']['receiver']['address'] = $rcAddress;
        $sendData['data']['receiver']['longitude'] = $rcLongitude;
        $sendData['data']['receiver']['latitude'] = $rcLatitude;
        $sendData['data']['counts'] = 1;
        $sendData['data']['orderType'] = 1;
        $sendData['data']['fromType'] = "weixin-xcx";
        $sendData['data']['orderTime'] = date("Y-m-d H:i:s");
        $sendData['data']['sendTime'] = date("Y-m-d H:i:s");
        $sendData['data']['payType'] = 2;
        $sendData['data']['isInvoice'] = false;
        $sendData['data']['productPrice'] = $productPrice;
        $sendData['data']['deliveryFee'] = $deliveryFee;
        $sendData['data']['mealFee'] = $mealFee;
        $sendData['data']['price'] = $price;
        $sendData['data']['merchantPrice'] = $merchantPrice;
        $sendData['data']['discountPrice'] = $discountPrice;
        $sendData['data']['weight'] = $weight;
        $sendData['data']['products'] = $products;

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.rider.previewDeliveryFee";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //呼叫聚合配送
    public function postDeliveryInfo($storeName, $subStoreId, $orderId, $deliveryOrderId, $sdName, $sdPhone, $address, $sdLongitude, $sdLatitude, $rcName, $rcPhone,
                                     $rcAddress, $rcLongitude, $rcLatitude, $productPrice, $deliveryFee, $mealFee, $price, $merchantPrice, $discountPrice, $weight, $products){
        $sendData['data']['deliverySysType'] = "foops";
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['data']['orderId'] = $orderId;
        $sendData['data']['storeName'] = $storeName;
        $sendData['data']['deliveryOrderId'] = $deliveryOrderId;
        $sendData['data']['sender']['name'] = $sdName;
        $sendData['data']['sender']['phone'] = $sdPhone;
        $sendData['data']['sender']['address'] = $address;
        $sendData['data']['sender']['longitude'] =  $sdLongitude;
        $sendData['data']['sender']['latitude'] = $sdLatitude;
        $sendData['data']['receiver']['name'] = $rcName;
        $sendData['data']['receiver']['phone'] = $rcPhone;
        $sendData['data']['receiver']['address'] = $rcAddress;
        $sendData['data']['receiver']['longitude'] = $rcLongitude;
        $sendData['data']['receiver']['latitude'] = $rcLatitude;
        $sendData['data']['counts'] = 1;
        $sendData['data']['orderType'] = 1;
        $sendData['data']['fromType'] = "weixin-xcx";
        $sendData['data']['orderTime'] = date("Y-m-d H:i:s");
        $sendData['data']['sendTime'] = date("Y-m-d H:i:s");
        $sendData['data']['payType'] = 2;
        $sendData['data']['isInvoice'] = false;
        $sendData['data']['productPrice'] = $productPrice;
        $sendData['data']['deliveryFee'] = $deliveryFee;
        $sendData['data']['mealFee'] = $mealFee;
        $sendData['data']['price'] = $price;
        $sendData['data']['merchantPrice'] = $merchantPrice;
        $sendData['data']['discountPrice'] = $discountPrice;
        $sendData['data']['weight'] = $weight;
        $sendData['data']['products'] = $products;

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.rider.postDeliveryInfo";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }
    //查询骑手位置
    public function getRiderPosition($subStoreId, $orderId, $extOrderId){
        $sendData['data']['deliverySysType'] = "foops";
        $sendData['data']['orderId'] = $orderId;
        $sendData['data']['extOrderId'] = $extOrderId;
        $sendData['data']['subStoreId'] = $subStoreId;

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.rider.getRiderPosition";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //获取充值链接
    public function getRechargeUrl($subStoreId){
        $sendData['data']['deliverySysType'] = "foops";
        $sendData['data']['subStoreId'] = $subStoreId;
        $sendData['data']['rechargePageType'] = 1;
        $sendData['data']['redirectUrl'] = "www.candao.com";

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.rider.getRechargeUrl";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

    //同步配送状态 - 取消
    public function cancelDeliveryOrder($orderId){
        $sendData['data']['deliverySysType'] = "foops";
        $sendData['data']['orderId'] = $orderId;
        $sendData['data']['driverStatus'] = 15;
        $sendData['data']['reason'] = "用户取消";

        $sendData['accessKey'] = $this->AccessKey;
        $sendData['timestamp'] = floatval($this->getMillisecond());
        $sendData['ticket'] = $this->generateUUID();
        $sendData['actionName'] = "candao.rider.updateDeliveryStatus";
        $sendData['serviceType'] = "pos";
        $sendData['sign'] = md5($sendData['accessKey'].$sendData['actionName'].$this->AccessSecret.$sendData['timestamp'].json_encode($sendData['data'],JSON_UNESCAPED_UNICODE));

        $data = json_encode($sendData, JSON_UNESCAPED_UNICODE);
        return $this->getInit($this->HttpUrl,"POST", $data);
    }

}
```

